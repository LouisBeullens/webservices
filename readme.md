### git

#### git instaleren

1. > apt-get install git

#### git repository downloaden

1. > git clone https://bitbucket.org/louisbeullens/webservices.git

### let's encrypt

#### certbot instaleren op debian-jessie

1. apt sources.list aanpassen
 1. open /etc/apt/sources.list
 2. lijn "deb http://ftp.debian.org/debian jessie-backports main" toevoegen aan einde van bestand.
2. > apt-get update
3. > apt-get install -y certbot -t jessie-backports

#### certificate sign request + private key aanmaken

1. > mkdir openssl
2. > cd openssl
3. > mkdir private
4. > chmod 700 private/
5. > openssl req -out cert.csr.pem -nodes -keyout private/cert.key.pem -newkey rsa:2048 -new
 
#### certificaat aanvragen bij let's encrypt

1. > certbot certonly --csr cert.csr.pem
2. kies voor standalone indien er geen webserver in het lijstje staat.

certbot zal de eerste keer vragen naar een e-mail adres voor key-recovery of key-revocation te melden.

### nodejs instaleren

#### met curl

1. > curl -sL https://deb.nodesource.com/setup_6.x | bash -
2. > apt-get install nodejs

#### met wget

1. > wget https://deb.nodesource.com/setup_6.x --no-check-certificate
2. > bash setup_6.x
3. > apt-get install nodejs

### nodejs loopback framework instaleren

1. > npm install -g strongloop

Ik heb voor loopback gekozen omdat het een krachtig framework is dat werkt met models en database abstractie. via json config files die aangemaakt worden met de cli van loopback kan eenvoudig gekozen worden aan welke database infrastructuur
je een model wilt koppelen verder maakt loopback op basis van de model properties automatische REST endpoints aan en kan ve via de cli relaties tussen de verschillende models leggen. met de explorer component (die standaard ingeshakeld is maar
je eventueel wel kunt uitschakelen) kan je de mogelikheden van de REST api verkennen. Als extra (maar dit valt buiten het bereik van deze opgave) kan je via officiele OAuth standaard access tokens verkrijgen door het toevoegen van de OAuth module

### nieuw loopback project aanmaken

1. > slc loopback
2. kies de naam van uw project
3. kies de map voor uw project (indien de naam niet gelijk is aan de naam van de huidige map zal loopback voorstelen een nieuwe map te maken)
3. versie selecteren, ik heb gekozen voor 2.x (stable) andere keuze is 3.x (beta)
4. kies het type van uw project, ik heb gekozen voor een api-server (een rest api met authenticatie d.m.v. access tokens)

loopback maakt nu de npm package.json file aan en installeert de nodige dependencies.

### certificaat installeren in loopback

om ssl te installeren bij loopback heb ik mij gebaseer op het serverscript van een officiele strongloop git repo
[https://github.com/strongloop/loopback-example-ssl/blob/master/server/server.js](https://github.com/strongloop/loopback-example-ssl/blob/master/server/server.js)

dit vervangt server.js in server/ map

server.js maakt gebruik van volgende symlinks zodat het certificaat en de private key niet in de repo bewaard moeten worden.

symlink maken > ln -s path_to_real_file simlink_name

* path_to_certificate_file.pem -> cert.crt.pem
* path_to_key_file.pem -> cert.key.pem

### loopback instellen als webserver

Alle bestanden die niets te maken hebben met nodejs kunnen in de client map geplaats worden, loopback haalt dan automatisch routes die het niet kent van de client map indien aan onderstaande configuratie voldaan is

boot/root.js uitschakelen door de js extensie te verwijderen

1. open server/middleware.json
2. volgende elementen toevoegen aan de files configuratie

> "loopback#static": {  
>   "params": "$!../client"  
> }  

### mariadb-server installeren

1. apt-get install mariadb-server
2. kies een root password

Ik heb voor mariadb gekozen omdat dit de open community versie is van mysql.

### phpmyadmin installeren

1. apt-get install phpmyadmin
2. tijdens de installatie kan je kiezen voor welke webservers je phpmyadmin wilt installeren (apache2/lighttp)
3. tijdens de installatie zal er ook gevraagd worden of mysql geconfigureerd moet worden ik heb ervoor gekozen om dit niet te doen via phpmyadmin

### loopback koppelen aan mysql

1. slc loopback:datasource
2. kies een naam voor uw datasource
3. kies het type van uw datasource (mongoDb/mysql/...) Ik heb gekozen voor mysql
4. de eerste keer zal loopback vragen om de connector (nodejs onderdeel van loopback) te installeren en toe te voegen aan de dependencies kies voor yes

### model toevoegen aan loopback

via het commando slc loopback:model kan je gemakkelijk een model toevoegen aan loopback.

Ik zal in dit voorbeeld het model topics toevoegen via de mysqldb datasource

zie models.txt in de root van de git repo voor meer informatie ivm andere models.

eens de model editor open is zal het voor een naam en een datasource vragen.

Als modelBase pak je best PeristedModel dit is een model waarbij loopback automatisch property api endpoints maakt

om te gebruiken.

Ook moet je aangeven of je, je model beschikbaar wilt stellen via de REST api. doe dit voor publieke models, het is niet nodig voor interne models die bijvoorbeeld gebruikt worden om manyToMany relaties te vormen.

Er wordt ook gevraagd of het een common of een server model is, het wordt aangeraden om hier voor common te kiezen indien er andere technologieŽn zijn die voor de frontend gebruik kunnen maken van deze models.

Nu kunnen we properties toevoegen er zal telkens voor een naam, type en of deze property aanwezig moet zijn (required Y/n), eventueel kan je een standaardwaarde megeven.

> topic inherits loopback:persistedModel  
> string: onderwerp  required
> user: eigenaar  
> date: startdatum  
> bool: prive  
> group: groep  
> HasMany: posts  

voeg properties onderwerp,startdatum en prive toe
user,group zijn foreign keys voor relaties.

via migratemodels.js kan je deze tabel aanmaken in de mysql database en eventueel bevolken met standaardwaarden elke keer loopback start.

### relaties leggen met loopback

gebruik het commando slc loopback:relation

1. kies het model waarop de relatie moet worden gelegd. (topic)
2. kies de relatie (hasmany)
3. (post)
4. kies de property voor de relatie (standaard laten: posts)
5. eventueel kan je een eigen foreign key gebruiken.
6. HasMany <-> BelongsToOne heeft geen hasThrough model nodig.

doe hetzelfde met het post model (BelongsTo)
